/*
* 2.6 Reducció de dígits | Seqüència d’asteriscos
* David Gómez | DAM1r
* 15/01/2023
*/

import java.util.*

fun printAsteriscs (num:Int) {
    if (num > 1){
        printAsteriscs(num-1)
    }
    for (i in 1 .. num){
        print("*")
    }
    println()
    if (num>1){
        printAsteriscs(num-1)
    }
}

fun main() {
    val scan = Scanner(System.`in`)
    println("Introdueix el nombre d'asteriscs:")
    val num = scan.nextInt()
    printAsteriscs(num)
}
