/*
* 2.2 Doble factorial | Exercicis de recursivitat
* David Gómez | DAM1r
* 14/12/20224
*/

import java.util.*

fun doubleFactorial (n:Int):Int{
    if (n == 1 || n == 0) return 1
    else return n * doubleFactorial(n-2)
}

fun main(){
    val scan = Scanner(System.`in`)
    println("Introdueix el número que vols calcular-li el doble factorial:")
    val factor = scan.nextInt()

    println("El resultat és: ${doubleFactorial(factor)}")
}
