/*
* 2.1 Factorial | Exercicis de recursivitat
* David Gómez | DAM1r
* 14/12/2022
*/

import java.util.*

fun factorial (n:Int):Int{
    if (n == 1 || n == 0) return 1
    else return n*factorial(n-1)
}

fun main(args: Array<String>) {


    val scan = Scanner(System.`in`)
    println("Introdueix el número que vols calcular-li el factorial:")
    val factor = scan.nextInt()

    println("El resultat és: ${factorial(factor)}")
}
