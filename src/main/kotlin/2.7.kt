/*
* 2.6 Reducció de dígits | Primers perfectes
* David Gómez | DAM1r
* 15/01/2023
*/
import java.util.*

fun reductionPrimer(n:Int):Int{
    if(n/10 > 0)return reductionPrimer(sumPrimer(n))
    else return n
}
fun sumPrimer(n:Int):Int{
    if (n/10 >0)return n%10 + sumPrimer(n/10)
    else return n
}
fun perfectPrimer(n:Int): Boolean {
    for(i in 2 until reductionPrimer(n)){
        if(reductionPrimer(n)%i == 0)return false
    }
    return true
}

fun main() {
    val scan = Scanner(System.`in`)
    println("Indica el enter, el qual vols comprobar si es primer perfecte")
    val n = scan.nextInt()
    println(perfectPrimer(n))
}

