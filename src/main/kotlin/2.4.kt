/*
* 2.4 Nombres creixents | Exercicis de recursivitat
* David Gómez | DAM1r
* 14/12/2022
*/

import java.util.*

fun growingNum (n:Int):Boolean{
    if (n/10>0) return n%10>(n/10)%10 && growingNum(n/10)
    else return true
}


fun main(){
    val scan = Scanner(System.`in`)
    println("Introdueix el número que vols comparar si es creixent:")
    val n = scan.nextInt()

    if (growingNum(n) == true){
        println("El resultat es creixent")
    }else
        println("El resultat no es creixent")
}

