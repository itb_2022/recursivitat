/*
* 2.3 Nombre de dígits | Exercicis de recursivitat
* David Gómez | DAM1r
* 14/12/2022
*/

import java.util.*

fun numberDigits (n:Int):Int{
    if (n>0) return 1 + numberDigits(n/10)
    else return 0
}

fun main(){
    val scan = Scanner(System.`in`)
    println("Introdueix el número que vols contar quants valors té:")
    val n = scan.nextInt()

    println("El resultat és: ${numberDigits(n)}")
}