/*
* 2.5 Reducció de dígits | Exercicis de recursivitat
* David Gómez | DAM1r
* 14/12/2022
*/

import java.util.*

fun sumCompostNumbersIndividually (n:Int):Int{
    if (n>0) return n%10 + sumCompostNumbersIndividually(n/10)
    else return 0
}

fun reductPrevFun (n:Int):Int{
    if (sumCompostNumbersIndividually(n)/10>0) return reductPrevFun(sumCompostNumbersIndividually(n))
    else return sumCompostNumbersIndividually(n)
}

fun main(){
    val scan = Scanner(System.`in`)
    println("Introdueix el número que vols calcular-li la reducció de digits:")
    val n = scan.nextInt()

    //println("El resultat és: ${sumCompostNumbersIndividually(n)}")
    println("El final resultat és: ${reductPrevFun(n)}")
}